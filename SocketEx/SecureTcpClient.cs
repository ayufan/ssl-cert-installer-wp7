﻿using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using Org.BouncyCastle.Crypto.Tls;
using Org.BouncyCastle.Asn1.X509;

namespace SocketEx
{
    public class AlwaysValidVerifyerSaved : ICertificateVerifyer, IEnumerable<X509CertificateStructure>
    {
        public AlwaysValidVerifyerSaved()
        {
        }

        private List<X509CertificateStructure> certs = new List<X509CertificateStructure>();

        public bool IsValid(X509CertificateStructure[] certs)
        {
            this.certs.AddRange(certs);
            return true;
        }

        public IEnumerator<X509CertificateStructure> GetEnumerator()
        {
            return certs.GetEnumerator();
        }

        System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
        {
            return certs.GetEnumerator();
        }
    }

    public class SecureTcpClient : TcpClient
    {
        private readonly TlsClient tlsClient;
        private Stream secureStream;
        private AlwaysValidVerifyerSaved verifier;

        public SecureTcpClient(string host, int port)
            : base(AddressFamily.InterNetwork)
        {
            this.verifier = new AlwaysValidVerifyerSaved();
            this.tlsClient = new LegacyTlsClient(this.verifier);

            var myEndpoint = new DnsEndPoint(host, port);
            InnerConnect(myEndpoint);
        }

        public SecureTcpClient(string host, int port, TlsClient tlsClient)
            : base(AddressFamily.InterNetwork)
        {
            this.tlsClient = tlsClient;

            var myEndpoint = new DnsEndPoint(host, port);
            InnerConnect(myEndpoint);
        }

        public AlwaysValidVerifyerSaved GetVerifier()
        {
            return verifier;
        }

        public override Stream GetStream()
        {
            return secureStream == null ? base.GetStream() : this.secureStream;
        }

        protected override void HandleConnectionReady()
        {
            var stream = this.GetStream();

            var handler = new TlsProtocolHandler(stream);
            handler.Connect(tlsClient);

            this.secureStream = handler.Stream;
        }
    }
}
