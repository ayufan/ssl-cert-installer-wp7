﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Org.BouncyCastle.Asn1.X509;
using System.Threading;
using System.Collections.ObjectModel;
using SocketEx;
using System.Linq;

namespace SslCertInstaller
{
    public delegate void CustomHandler<TSender, TEventArgs>(TSender sender, TEventArgs e);

    public class SiteVerifier
    {
        public Uri Uri;

        public event CustomHandler<SiteVerifier, HttpWebResponse> HttpCompleted;
        public event CustomHandler<SiteVerifier, WebException> HttpErrorOccurred;
        public event CustomHandler<SiteVerifier, X509CertificateStructure[]> Completed;
        public event CustomHandler<SiteVerifier, Exception> ErrorOccurred;

        public SiteVerifier()
        {
        }

        public void Verify(Uri uri)
        {
            if (this.Uri != null)
                throw new InvalidOperationException("create new object");

            this.Uri = uri;
            BeginHttpRequest();
        }

        private void BeginHttpRequest()
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Uri);
            request.Method = "GET";
            request.BeginGetResponse(EndHttpRequest, request);
        }

        private void EndHttpRequest(IAsyncResult result)
        {
            HttpWebRequest request = (HttpWebRequest)result.AsyncState;

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.EndGetResponse(result);
                response.Close();

                if (HttpCompleted != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(HttpCompleted, this, response);
                }
            }
            catch (WebException ex)
            {
                if (HttpErrorOccurred != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(HttpErrorOccurred, this, ex);
                }
            }

            ThreadPool.QueueUserWorkItem(VerifySite);
        }

        private void VerifySite(object siteUriObject)
        {
            try
            {
                using (var connection = new SecureTcpClient(Uri.Host, Uri.Port))
                {
                    X509CertificateStructure[] certs = connection.GetVerifier().ToArray();

                    if (Completed != null)
                    {
                        Deployment.Current.Dispatcher.BeginInvoke(Completed, this, certs);
                    }
                }
            }
            catch (Exception ex)
            {
                if (ErrorOccurred != null)
                {
                    Deployment.Current.Dispatcher.BeginInvoke(ErrorOccurred, this, ex);
                }
            }
        }

        public bool MatchesCommonName(string cn)
        {
            string[] uris = Uri.Host.Split('.');
            string[] cns = cn.Split('.');

            if (uris.Length != cns.Length)
                return false;

            for (int i = 0; i < uris.Length; ++i)
            {
                if (cns[i] == "*")
                    continue;
                if (!uris[i].Equals(cns[i], StringComparison.CurrentCultureIgnoreCase))
                    return false;
            }
            return true;
        }
    }
}
