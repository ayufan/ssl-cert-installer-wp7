﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Tasks;
using System.IO;
using System.IO.IsolatedStorage;
using System.Net.Browser;
using System.Text;
using SocketEx;
using Org.BouncyCastle.Asn1.X509;
using System.Diagnostics;
using System.Threading;
using System.Collections.ObjectModel;

namespace SslCertInstaller
{
    public partial class MainPage : PhoneApplicationPage
    {
        private SiteVerifier siteVerifier = null;
        public ObservableCollection<LogInfo> logs = new ObservableCollection<LogInfo>();
        public ObservableCollection<X509CertificateStructure> certificates = new ObservableCollection<X509CertificateStructure>();
        public ObservableCollection<RecentInfo> recent;
        private bool navigatedBack = false;

        // Constructor
        public MainPage()
        {
            if (!IsolatedStorageSettings.ApplicationSettings.TryGetValue("recentInfo", out recent))
            {
                recent = new ObservableCollection<RecentInfo>();
                IsolatedStorageSettings.ApplicationSettings["recentInfo"] = recent;
            }

            InitializeComponent();

            if (recent.Count != 0)
                SiteUri.Text = recent[0].ToString();
            SiteLog.ItemsSource = logs;
            SiteCertificates.ItemsSource = certificates;
            RecentSites.ItemsSource = recent;
        }

        ~MainPage()
        {
        }

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
            base.OnNavigatedTo(e);

            if (!string.IsNullOrWhiteSpace(SiteUri.Text) && navigatedBack)
            {
                navigatedBack = false;
                ButtonVerify_Click(this, new RoutedEventArgs());
            }
        }

        private void CloseSiteVerifier()
        {
            if (siteVerifier != null)
            {
                siteVerifier.HttpErrorOccurred -= siteVerifier_HttpErrorOccurred;
                siteVerifier.HttpCompleted -= siteVerifier_HttpCompleted;
                siteVerifier.ErrorOccurred -= siteVerifier_ErrorOccurred;
                siteVerifier.Completed -= siteVerifier_Completed;
                siteVerifier = null;
            }
        }

        private void ButtonVerify_Click(object sender, RoutedEventArgs e)
        {
            CloseSiteVerifier();

            VerifySiteIndicator.IsIndeterminate = true;
            ButtonOpen.IsEnabled = false;
            ButtonInstall.IsEnabled = false;
            logs.Clear();
            certificates.Clear();

            try
            {
                Uri uri = null;

                if (!Uri.TryCreate(SiteUri.Text, UriKind.Absolute, out uri))
                {
                    if (!Uri.TryCreate("https://" + SiteUri.Text, UriKind.Absolute, out uri))
                    {
                        throw new ArgumentException("Invalid URI. Please enter: https://site.name.com/");
                    }
                }

                RecentInfo recentInfo = new RecentInfo() { Uri = uri };
                recent.Remove(recentInfo);
                recent.Insert(0, recentInfo);
                IsolatedStorageSettings.ApplicationSettings.Save();

                SiteUri.Text = uri.ToString();

                siteVerifier = new SiteVerifier();
                siteVerifier.HttpErrorOccurred += siteVerifier_HttpErrorOccurred;
                siteVerifier.HttpCompleted += siteVerifier_HttpCompleted;
                siteVerifier.ErrorOccurred += siteVerifier_ErrorOccurred;
                siteVerifier.Completed += siteVerifier_Completed;
                siteVerifier.Verify(uri);
            }
            catch (Exception ex)
            {
                VerifySiteIndicator.IsIndeterminate = false;
                siteVerifier = null;
                MessageBox.Show(ex.Message);
            }
        }

        void siteVerifier_Completed(SiteVerifier sender, X509CertificateStructure[] e)
        {
            bool canInstall = true;

            if (e.Length == 0)
            {
                logs.Add(new LogInfo(LogType.Error, "No certificates found. Unable to install."));
                canInstall = false;
            }
            else
            {
                foreach (X509CertificateStructure cert in e)
                {
                    certificates.Add(cert);
                }

                X509CertificateStructure self = e[0];
                bool cnVerified = false;
                // 2.5.29.17
                foreach (string cn in self.Subject.GetValueList(X509Name.CN))
                {
                    if (sender.MatchesCommonName(cn))
                    {
                        cnVerified = true;
                    }
                }

#if ENABLE_SUBJECTALTNAME
                var subjectAltName = self.TbsCertificate.Extensions.GetExtension(X509Extensions.SubjectAlternativeName);
                if (subjectAltName != null)
                {
                    using (StreamReader reader = new StreamReader(subjectAltName.Value.Parser.GetOctetStream()))
                    {
                        while (!reader.EndOfStream)
                        {
                            string altName = reader.ReadLine();
                            if (altName.StartsWith("DNS="))
                            {

                            }
                        }
                    }
                }
#endif

                if (!cnVerified)
                {
                    logs.Add(new LogInfo(LogType.Error, "Certificate's Common Name doesn't match the host name. Unable to install."));
                    canInstall = false;
                }
                else
                {
                    logs.Add(new LogInfo(LogType.Good, "Certificate's Common Name match the host name."));
                }

                foreach (X509CertificateStructure cert in e)
                {
                    if (DateTime.Now < cert.StartDate.ToDateTime() ||
                        cert.EndDate.ToDateTime() < DateTime.Now)
                    {
                        logs.Add(new LogInfo(LogType.Error, "Certificate '" + cert.Subject.ToString() + "' is expired. Unable to install."));
                        canInstall = false;
                    }
                }

                X509CertificateStructure lastCert = e[e.Length - 1];

                if (lastCert.Issuer.Equivalent(lastCert.Subject))
                {
                    logs.Add(new LogInfo(LogType.Good, "Root certificate is self signed."));
                }
                else
                {
                    logs.Add(new LogInfo(LogType.Warning, "Root certificate not found. Unable to install."));
                    canInstall = false;
                }
            }

            ButtonInstall.IsEnabled = canInstall;
            VerifySiteIndicator.IsIndeterminate = false;
        }

        void siteVerifier_ErrorOccurred(SiteVerifier sender, Exception e)
        {
            logs.Add(new LogInfo(LogType.Error, "We couldn't connect to specified site. Other application's may not work correctly."));
            VerifySiteIndicator.IsIndeterminate = false;
        }

        void siteVerifier_HttpCompleted(SiteVerifier sender, HttpWebResponse e)
        {
            RecentInfo recentInfo = new RecentInfo() { Uri = sender.Uri, Valid = true };
            recent.Remove(recentInfo);
            recent.Insert(0, recentInfo);
            IsolatedStorageSettings.ApplicationSettings.Save();

            logs.Add(new LogInfo(LogType.Good, "Site was verified using built-in certificates. Other application's should work correctly."));
            ButtonOpen.IsEnabled = true;
        }

        void siteVerifier_HttpErrorOccurred(SiteVerifier sender, WebException e)
        {
            logs.Add(new LogInfo(LogType.Error, "Site cannot be verified using built-in certificates. You may have to install certificate on the device."));
        }

        private void CertItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement elem = sender as FrameworkElement;
            if (elem == null)
                return;
            X509CertificateStructure cert = elem.Tag as X509CertificateStructure;
            if (cert == null)
                return;

            InstallCertificate(cert);
        }

        public void InstallCertificate(params X509CertificateStructure[] cert)
        {
            if (VerifySiteIndicator.IsIndeterminate)
                return;

            VerifySiteIndicator.IsIndeterminate = true;

            try
            {
                cert.InstallCertificate(this.CertificateInstalled);
                navigatedBack = true;
            }
            catch (Exception)
            {
                VerifySiteIndicator.IsIndeterminate = false;
            }
        }

        private void CertificateInstalled(X509CertificateStructure[] certificate, Exception exception)
        {
            if (exception != null)
            {
                MessageBox.Show(exception.Message);
            }
            else
            {
                navigatedBack = true;
            }
            VerifySiteIndicator.IsIndeterminate = false;
        }

        private void ButtonInstall_Click(object sender, RoutedEventArgs e)
        {
            if (VerifySiteIndicator.IsIndeterminate)
                return;

            if (certificates.Count == 0)
                return;

            InstallCertificate(certificates.Reverse().ToArray());
        }

        private void ButtonOpen_Click(object sender, RoutedEventArgs e)
        {
            if (siteVerifier == null)
                return;

            try
            {
                WebBrowserTask webBrowserTask = new WebBrowserTask();
                webBrowserTask.Uri = siteVerifier.Uri;
                webBrowserTask.Show();
            }
            catch (Exception)
            {
            }
        }

        private void RecentItem_Tap(object sender, System.Windows.Input.GestureEventArgs e)
        {
            FrameworkElement elem = sender as FrameworkElement;
            if (elem == null)
                return;
            RecentInfo uri = elem.Tag as RecentInfo;
            if (uri == null)
                return;

            SiteUri.Text = uri.ToString();
            ButtonVerify_Click(sender, new RoutedEventArgs());
            MainPivot.SelectedItem = VerifyItem;
        }

        private void Email_Click(object sender, RoutedEventArgs e)
        {
            EmailComposeTask emailComposeTask = new EmailComposeTask();
            emailComposeTask.Subject = "SSL Certificate Installer";
            emailComposeTask.To = "ayufan@ayufan.eu";
            emailComposeTask.Show();
        }

        private void WebSite_Click(object sender, RoutedEventArgs e)
        {
            WebBrowserTask webBrowserTask = new WebBrowserTask();
            webBrowserTask.Uri = new Uri("http://ayufan.eu", UriKind.Absolute);
            webBrowserTask.Show();
        }

        private void LinkRate_Click(object sender, RoutedEventArgs e)
        {
            new MarketplaceReviewTask().Show();
        }
    }
}