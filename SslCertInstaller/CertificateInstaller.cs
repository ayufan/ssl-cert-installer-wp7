﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Org.BouncyCastle.Asn1.X509;
using System.IO;
using Microsoft.Phone.Tasks;

namespace SslCertInstaller
{
    public delegate void CertificateInstallerCallback(X509CertificateStructure[] certificate, Exception exception);

    public static class CertificateInstaller
    {
        private class CertificateInstallerDescription
        {
            public HttpWebRequest Request;
            public X509CertificateStructure[] Certificate;
            public CertificateInstallerCallback Callback;
        }

        private static void GetRequestStream(IAsyncResult result)
        {
            CertificateInstallerDescription request = (CertificateInstallerDescription)result.AsyncState;

            try
            {
                using (Stream postRequest = request.Request.EndGetRequestStream(result))
                {
                    using (StreamWriter writer = new StreamWriter(postRequest))
                    {
                        for (int i = 0; i < request.Certificate.Length; ++i)
                        {
                            byte[] encoded = request.Certificate[i].GetDerEncoded();
                            string base64 = Convert.ToBase64String(encoded);
                            if (i == 0)
                                writer.Write("der={1}", i, HttpUtility.UrlEncode(base64));
                            else
                                writer.Write("&der{0}={1}", i, HttpUtility.UrlEncode(base64));
                        }
                    }
                }
                request.Request.BeginGetResponse(GetResponseCallback, request);
            }
            catch (Exception ex)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => request.Callback(request.Certificate, ex));
            }
        }

        private static void GetResponseCallback(IAsyncResult result)
        {
            CertificateInstallerDescription request = (CertificateInstallerDescription)result.AsyncState;

            try
            {
                HttpWebResponse response = (HttpWebResponse)request.Request.EndGetResponse(result);

                using (Stream streamResponse = response.GetResponseStream())
                {
                    using (StreamReader streamReader = new StreamReader(streamResponse))
                    {
                        var responseText = streamReader.ReadToEnd();

                        if (responseText.StartsWith("http://"))
                        {
                            WebBrowserTask webBrowserTask = new WebBrowserTask();
                            webBrowserTask.Uri = new Uri(responseText, UriKind.Absolute);
                            webBrowserTask.Show();
                        }
                        else
                        {
                            throw new ArgumentException(responseText);
                        }
                    }
                }
                response.Close();

                Deployment.Current.Dispatcher.BeginInvoke(() => request.Callback(request.Certificate, null));
            }
            catch (WebException ex)
            {
                Deployment.Current.Dispatcher.BeginInvoke(() => request.Callback(request.Certificate, ex));
            }
        }

        public static void InstallCertificate(this X509CertificateStructure[] certificate, CertificateInstallerCallback callback)
        {
            CertificateInstallerDescription desc = new CertificateInstallerDescription();

            desc.Certificate = certificate;
            desc.Callback = callback;
            desc.Request = (HttpWebRequest)WebRequest.Create("http://ayufan.eu/local/ssl/upload.php");
            desc.Request.Method = "POST";
            desc.Request.ContentType = "application/x-www-form-urlencoded";
            desc.Request.BeginGetRequestStream(GetRequestStream, desc);
        }
    }
}
