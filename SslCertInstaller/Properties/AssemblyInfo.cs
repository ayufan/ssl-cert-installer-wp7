﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Resources;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("SSL Untrusted Installer")]
[assembly: AssemblyDescription("Application allows to install untrusted ssl certificates directly from phone")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("http://ayufan.eu")]
[assembly: AssemblyProduct("SSL Untrusted Installer")]
[assembly: AssemblyCopyright("Copyright © ayufan 2012")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("39a39175-bb0f-4ce1-b85d-ce04a262de20")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("1.1.0")]
[assembly: AssemblyFileVersion("1.1.0")]
[assembly: NeutralResourcesLanguageAttribute("en-US")]
