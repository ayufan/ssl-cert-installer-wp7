﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace SslCertInstaller
{
    public enum LogType
    {
        Error,
        Warning,
        Info,
        Good
    }

    public class LogInfo
    {
        public LogType Type { get; set; }
        public string Message { get; set; }

        public Brush StatusColor
        {
            get
            {
                switch (Type)
                {
                    case LogType.Error:
                        return new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));

                    case LogType.Warning:
                        return new SolidColorBrush(Color.FromArgb(255, 255, 255, 0));

                    case LogType.Good:
                        return new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));

                    default:
                        return new SolidColorBrush(Color.FromArgb(255, 255, 255, 255));
                }
            }
        }

        public string StatusImage
        {
            get
            {
                switch (Type)
                {
                    case LogType.Error:
                        return @"images\fatal.png";

                    case LogType.Warning:
                        return @"images\warning.png";

                    case LogType.Good:
                        return @"images\ok.png";

                    default:
                        return @"images\null.png";
                }
            }
        }

        public string Status
        {
            get { return Type.ToString(); }
        }

        public LogInfo(LogType type, string message)
        {
            this.Type = type;
            this.Message = message;
        }
    }

    public class RecentInfo
    {
        public Uri Uri { get; set; }
        public bool Valid { get; set; }

        public string StatusImage
        {
            get
            {
                if (Valid)
                    return @"images\ok.png";
                else
                    return @"images\fatal.png";
            }
        }

        public string Status
        {
            get
            {
                if (Valid)
                    return "Good";
                else
                    return "Not working";
            }
        }

        public Brush StatusColor
        {
            get
            {
                if (Valid)
                    return new SolidColorBrush(Color.FromArgb(255, 0, 255, 0));
                else
                    return new SolidColorBrush(Color.FromArgb(255, 255, 0, 0));
            }
        }

        public override string ToString()
        {
            if (Uri != null)
                return Uri.ToString();
            return "";
        }

        public override int GetHashCode()
        {
            if (Uri != null)
                return Uri.GetHashCode();
            return 0;
        }

        public override bool Equals(object obj)
        {
            if (obj is RecentInfo)
            {
                return ((RecentInfo)obj).Uri == Uri;
            }
            return false;
        }
    }
}
